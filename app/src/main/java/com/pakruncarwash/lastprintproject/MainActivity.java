package com.pakruncarwash.lastprintproject;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pakruncarwash.lastprintproject.Util.BluetoothUtil;
import com.pakruncarwash.lastprintproject.Util.ESCUtil;
import com.pakruncarwash.lastprintproject.Util.SunmiPrintHelper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class MainActivity extends AppCompatActivity {
    EditText etmain;
    Button print,testprint;
    private String[] mStrings = new String[]{"CP437", "CP850", "CP860", "CP863", "CP865", "CP857", "CP737", "CP928", "Windows-1252", "CP866", "CP852", "CP858", "CP874", "Windows-775", "CP855", "CP862", "CP864", "GB18030", "BIG5", "KSC5601", "utf-8"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etmain=findViewById(R.id.etmain);
        print=findViewById(R.id.print);
        testprint=findViewById(R.id.testprint);
        SunmiPrintHelper.getInstance().initSunmiPrinterService(this);
        BluetoothUtil.sendData(ESCUtil.init_printer());

        print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    printmain(etmain.getText().toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        testprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    testprintfun();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private void testprintfun() throws IOException {
        String data="پاکران کارواش\n--------------------\n" +
                " test \n" +
                " cashier : Mehdi Pour \n-------------------" +
                "\ncar: santafe " +
                "\n package :    eco\n" +
                "--------------\n" +
                " ممنون از اینکه مارا انتخاب کردید";


            int record = 17;

            BluetoothAdapter btAdapter = BluetoothUtil.getBTAdapter();
            BluetoothDevice device = BluetoothUtil.getDevice(btAdapter);
            BluetoothUtil.getSocket(device);
            BluetoothUtil.connectBlueTooth(this);


            try {
                //BluetoothUtil.sendData(a, socket);
                BluetoothUtil.sendData(ESCUtil.boldOff());
                BluetoothUtil.sendData(ESCUtil.underlineOff());
                BluetoothUtil.sendData(ESCUtil.singleByteOff());
                BluetoothUtil.sendData(ESCUtil.setCodeSystem(codeParse(record)));
                BluetoothUtil.sendData(data.getBytes(mStrings[record]));
                BluetoothUtil.sendData(ESCUtil.nextLine(3));
            } catch (IOException e) {
                e.printStackTrace();
            }

    }
    private byte codeParse(int value) {
        byte res = 0x00;
        switch (value) {
            case 0:
                res = 0x00;
                break;
            case 1:
            case 2:
            case 3:
            case 4:
                res = (byte) (value + 1);
                break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                res = (byte) (value + 8);
                break;
            case 12:
                res = 21;
                break;
            case 13:
                res = 33;
                break;
            case 14:
                res = 34;
                break;
            case 15:
                res = 36;
                break;
            case 16:
                res = 37;
                break;
            case 17:
            case 18:
            case 19:
                res = (byte) (value - 17);
                break;
            case 20:
                res = (byte) 0xff;
                break;
            default:
                break;
        }
        return (byte) res;
    }

    private void printmain(String data) throws IOException {
        int record = 17;

        BluetoothAdapter btAdapter = BluetoothUtil.getBTAdapter();
        BluetoothDevice device = BluetoothUtil.getDevice(btAdapter);
        BluetoothUtil.getSocket(device);
        BluetoothUtil.connectBlueTooth(this);


        try {
            //BluetoothUtil.sendData(a, socket);
            BluetoothUtil.sendData(ESCUtil.boldOff());
            BluetoothUtil.sendData(ESCUtil.underlineOff());
            BluetoothUtil.sendData(ESCUtil.singleByteOff());
            BluetoothUtil.sendData(ESCUtil.setCodeSystem(codeParse(record)));
            BluetoothUtil.sendData(data.getBytes(mStrings[record]));
            BluetoothUtil.sendData(ESCUtil.nextLine(3));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
